<?php
class ventas
{
	private $pdo;
    
    public $CveArt;
    public $Descripcion;
    public $Precio;
    public $IVA;
    public $Descuento;
    public $Existencia;

	public function __CONSTRUCT()
	{
		try
		{
			$this->pdo = Database::StartUp();     
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Listar()
	{
		try
		{
			$result = array();

			$stm = $this->pdo->prepare("SELECT articulos.CveArt,articulos.Descripcion, articulos.Precio, existencia.Existencia FROM articulos , existencia WHERE articulos.CveArt = existencia.CveArt");
			$stm->execute();

			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e)
		{
			die($e->getMessage());
		}
	}

	public function Obtener($CveArt)
	{
		try 
		{
			$stm = $this->pdo
			          ->prepare("SELECT * FROM articulos WHERE CveArt = ?");
			          

			$stm->execute(array($CveArt));
			return $stm->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Eliminar($CveArt)
	{
		try 
		{
			$stm = $this->pdo
			            ->prepare("DELETE FROM articulos WHERE CveArt = ?");			          

			$stm->execute(array($CveArt));
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function Actualizar(ventas $data)
	{
		try 
		{
			$sql = "UPDATE articulos SET 
						Descripcion        = ?,
                        Precio        = ?,
						IVA            = ?, 
						Descuento = ?
				    WHERE CveArt = ?";

			$this->pdo->prepare($sql)
			     ->execute(
				    array(
                        $data->Descripcion, 
                        $data->Precio,
                        $data->IVA,
                        $data->Descuento,
                        $data->CveArt
					)
				);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}


	}

	public function Registrar(ventas $data)
	{
		try 
		{
		$sql = "INSERT INTO articulos (CveArt,Descripcion,Precio,IVA,Descuento) 
		        VALUES (?, ?, ?, ?, ?)";

		$this->pdo->prepare($sql)
		     ->execute(
				array(
					$data->CveArt,
					$data->Descripcion, 
					$data->Precio,
					$data->IVA,
					$data->Descuento					
                )
			);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}

		try 
		{
		$sql = "INSERT INTO existencia (CveArt,Existencia) 
		        VALUES (?, 0)";

		$this->pdo->prepare($sql)
		     ->execute(
				array(
					$data->CveArt					
                )
			);
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}

	public function VaciarTabla()
	{
		try 
		{
			$stm = $this->pdo
			            ->prepare("DELETE FROM articulos WHERE 1");			          

			$stm->execute();
		} catch (Exception $e) 
		{
			die($e->getMessage());
		}
	}
}