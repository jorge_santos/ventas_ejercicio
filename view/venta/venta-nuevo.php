<h1 class="page-header">
   Nuevo Registro
</h1>

<ol class="breadcrumb">
  <li><a href="?c=venta">Ventas</a></li>
  <li class="active">Nuevo Registro</li>
</ol>

<form action="?c=venta&a=Guardar" method="post" enctype="multipart/form-data">
       
    <div class="form-group">
        <label>Clave</label>
        <input type="text" name="CveArt"  class="form-control" placeholder="Ingrese la Clave del producto" />
    </div>
    
    <div class="form-group">
        <label>Descripcion</label>
        <input type="text" name="descripcion"  class="form-control" placeholder="Ingrese la Descripcion"/>
    </div>
    
    <div class="form-group">
        <label>Precio</label>
        <input type="text" name="precio" class="form-control" placeholder="Ingrese el precio"/>
    </div>

    <div class="form-group">
        <label>IVA</label>
        <input type="text" name="iva" class="form-control" placeholder="Ingrese el IVA" />
    </div>

    <div class="form-group">
        <label>Descuento</label>
        <input type="text" name="descuento" class="form-control" placeholder="Ingrese el Descuento"  />
    </div>
        
    <hr />
    
    <div class="text-right">
        <button class="btn btn-success"><img src="iconos/disco-flexible.png" style="width: 30px"></button>
    </div>
</form>