<h1 class="page-header">Ventas</h1>

<div class="well well-sm text-right">
    <a class="btn btn-primary" href="?c=venta&a=Nuevo"> <img src="iconos/add-file.png" style="width: 40px"></a>
</div>

<table class="table table-striped">
    <thead>
        <tr>
            <th >CveArt</th>
            <th style="width:180px;">Descripcion</th>
            <th>Precio</th>
            <th>Existencia</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($this->model->Listar() as $r): ?>
        <tr>
        <td><?php echo $r->CveArt; ?></td>
            <td><?php echo $r->Descripcion; ?></td>
            <td><?php echo $r->Precio; ?></td>
            <td><?php echo $r->Existencia; ?></td>

            <td>
                <a href="?c=venta&a=Modificar&CveArt=<?php echo $r->CveArt; ?>"><img src="iconos/editar.png" style="width: 30px"></a>
            </td>
            <td>
                <a href="?c=venta&a=Descripcion&CveArt=<?php echo $r->CveArt; ?>"><img src="iconos/etiqueta.png" style="width: 30px"></a>
            </td>
            <td>
                <a onclick="javascript:return confirm('¿Seguro de eliminar este registro?');" href="?c=venta&a=Eliminar&CveArt=<?php echo $r->CveArt; ?>"><img src="iconos/boton-eliminar.png" style="width: 30px"></a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table> 
