<h1 class="page-header">
    <?php echo $alm->CveArt != null ? $alm->Descripcion : 'Modificar Producto'; ?>
</h1>

<ol class="breadcrumb">
  <li><a href="?c=venta">Venta</a></li>
  <li class="active"><?php echo $alm->CveArt != null ? $alm->Descripcion : 'Descripcion'; ?></li>
</ol>
   
    <div class="form-group">
    <label >Clave del articulo: <?php echo $alm->CveArt; ?></label>
    </div>
    <div class="form-group">
    <label >Descripcion del articulo: <?php echo $alm->Descripcion; ?></label>
    </div>
    <div class="form-group">
    <label >Precio del articulo: $<?php echo $alm->Precio; ?></label>
    </div>
    <div class="form-group">
    <label >IVA del articulo: <?php echo $alm->IVA; ?>%</label>
    </div>
    <div class="form-group">
    <label >Descuento del articulo: <?php echo $alm->Descuento; ?>%</label>
    </div>

   
    
    <div class="text-right">
        <a class="btn btn-success" onclick="javascript:return" href="?c=venta&a=Index"><img src="iconos/casa.png" style="width: 30px"></a>
    </div>
