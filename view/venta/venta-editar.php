<h1 class="page-header">
    <?php echo $alm->CveArt != null ? $alm->Descripcion : 'Modificar Producto'; ?>
</h1>

<ol class="breadcrumb">
  <li><a href="?c=venta">Venta</a></li>
  <li class="active"><?php echo $alm->CveArt != null ? $alm->Descripcion : 'Modificar'; ?></li>
</ol>

<form id="frm-alumno" action="?c=venta&a=GuardarModificacion" method="post" enctype="multipart/form-data">
    <input type="hidden" name="CveArt" value="<?php echo $alm->CveArt; ?>" />
    <div class="form-group">
    <label >Clave del articulo: <?php echo $alm->CveArt; ?></label>
    
    </div>

    <div class="form-group">
        <label>Descripcion</label>        
        <input type="text" name="descripcion" value="<?php echo $alm->Descripcion; ?>" class="form-control" placeholder="Ingrese su nombre" data-validacion-tipo="requerido|min:3" />
    </div>
    
    <div class="form-group">
        <label>Precio</label>
        <input type="text" name="precio" value="<?php echo $alm->Precio; ?>" class="form-control" placeholder="Ingrese su apellido" data-validacion-tipo="requerido|min:1" />
    </div>
    
    <div class="form-group">
        <label>IVA</label>
        <input type="text" name="iva" value="<?php echo $alm->IVA; ?>" class="form-control" placeholder="Ingrese su correo electrónico" data-validacion-tipo="requerido|min:1" />
    </div>
    
    <div class="form-group">
        <label>Descuento</label>
        <input type="text" name="descuento" value="<?php echo $alm->Descuento ?>" class="form-control" placeholder="Ingrese su correo electrónico" data-validacion-tipo="requerido|min:1" />
    </div>
    
   

    <hr />
    
    <div class="text-right">
        <button class="btn btn-success"><img src="iconos/disco-flexible.png" style="width: 30px"></button>
    </div>
</form>

<script>
    $(document).ready(function(){
        $("#frm-alumno").submit(function(){
            return $(this).validate();
        });
    })
</script>