<?php
require_once 'model/venta.php';

class ventaController{
    
    private $model;
    
    public function __CONSTRUCT(){
        $this->model = new ventas();
    }
    
    public function Index(){
        require_once 'view/header.php';
        require_once 'view/venta/venta.php';
        require_once 'view/footer.php';
    }
    
    public function Crud(){
        $alm = new ventas();
        
        if(isset($_REQUEST['CveArt'])){
            $alm = $this->model->Obtener($_REQUEST['CveArt']);
        }
        
      
    }

    public function Modificar(){
        $alm = new ventas();
        
        if(isset($_REQUEST['CveArt'])){
            $alm = $this->model->Obtener($_REQUEST['CveArt']);
        }
        
        require_once 'view/header.php';
        require_once 'view/venta/venta-editar.php';
        require_once 'view/footer.php';
    }

    public function Descripcion(){
        $alm = new ventas();
        
        if(isset($_REQUEST['CveArt'])){
            $alm = $this->model->Obtener($_REQUEST['CveArt']);
        }
        
        require_once 'view/header.php';
        require_once 'view/venta/venta-descripcion.php';
        require_once 'view/footer.php';
    }

    public function Nuevo(){
        require_once 'view/header.php';
        require_once 'view/venta/venta-nuevo.php';
        require_once 'view/footer.php';
    }
    
    public function Guardar(){
        $alm = new ventas();
        
        $alm->CveArt = $_REQUEST['CveArt'];
        $alm->Descripcion = $_REQUEST['descripcion'];
        $alm->Precio = $_REQUEST['precio'];
        $alm->IVA = $_REQUEST['iva'];
        $alm->Descuento = $_REQUEST['descuento'];
        $this->model->Registrar($alm);

        header('Location: index.php');
    }

    public function GuardarModificacion(){
        $alm = new ventas();
        
        $alm->CveArt = $_REQUEST['CveArt'];
        $alm->Descripcion = $_REQUEST['descripcion'];
        $alm->Precio = $_REQUEST['precio'];
        $alm->IVA = $_REQUEST['iva'];
        $alm->Descuento = $_REQUEST['descuento'];
        $this->model->Actualizar($alm);

        header('Location: index.php');
    }
    
    public function Eliminar(){
        $this->model->Eliminar($_REQUEST['CveArt']);
        header('Location: index.php');
    }

    public function VaciarTabla(){
        $this->model->VaciarTabla();
        header('Location: index.php');
    }
}